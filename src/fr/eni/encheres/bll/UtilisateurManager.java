package fr.eni.encheres.bll;

import fr.eni.encheres.bll.bo.Utilisateur;
import fr.eni.encheres.dal.DAOFactory;

public class UtilisateurManager {
	
	public void inscriptionUtilisateur(Utilisateur utilisateur) {
		DAOFactory.getUtilisateurDAO().enregistrerUtilisateur(utilisateur);
	}

	public boolean verifMail(String email) {
		return DAOFactory.getUtilisateurDAO().verifEmail(email);
	}
	
	public Utilisateur connexionUtilisateur(String identifiant, String mdp, String recherche) {
		Utilisateur utilisateur = DAOFactory.getUtilisateurDAO().connexionUtilisateur(identifiant, mdp, recherche);
		return utilisateur;
	}

	public boolean verifPseudo(String pseudo) {
		// TODO Auto-generated method stub
		return  DAOFactory.getUtilisateurDAO().verifPseudo(pseudo);
	}

	public Utilisateur modificationUtilisateur(Utilisateur utilisateur) {
		// TODO Auto-generated method stub
		DAOFactory.getUtilisateurDAO().modificationUtilisateur(utilisateur);
		return utilisateur;
	}

	public Utilisateur recupereUtilisateurById(int id) {
		Utilisateur utilisateur = DAOFactory.getUtilisateurDAO().recupereUtilisateurById(id);
		return utilisateur;
	}
	

	
}
