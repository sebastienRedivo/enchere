package fr.eni.encheres.dal;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Time;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import fr.eni.encheres.bll.bo.Utilisateur;

public class UtilisateurDAO {

	public void enregistrerUtilisateur(Utilisateur utilisateur) {
		Connection cnx;
		try {
			cnx = ConnectionProvider.getConnection();
			PreparedStatement pstmt = cnx.prepareStatement(
					"INSERT INTO utilisateurs (pseudo, nom, prenom, email, telephone, rue, code_postal, ville, mot_de_passe, credit, administrateur)VALUES (?,?,?,?,?,?,?,?,?,?,?)");

			pstmt.setString(1, utilisateur.getPseudo());
			pstmt.setString(2, utilisateur.getNom());
			pstmt.setString(3, utilisateur.getPrenom());
			pstmt.setString(4, utilisateur.getEmail());
			pstmt.setString(5, utilisateur.getTelephone());
			pstmt.setString(6, utilisateur.getRue());
			pstmt.setString(7, utilisateur.getCodePostal());
			pstmt.setString(8, utilisateur.getVille());
			pstmt.setString(9, utilisateur.getMotDePasse());
			pstmt.setInt(10, 100);
			pstmt.setInt(11, 0);

			pstmt.executeUpdate();

			cnx.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	public boolean verifEmail(String email) {
		Connection cnx;
		boolean resultat = false;

		try {
			cnx = ConnectionProvider.getConnection();

			PreparedStatement pstmt = cnx.prepareStatement("SELECT email FROM utilisateurs WHERE email = ?");
			pstmt.setString(1, email);
			ResultSet rs = pstmt.executeQuery();
			System.out.println("DAO utilisateur 1");
			System.out.println(rs);
			if (rs.next()) {
				resultat = true;
			}
			System.out.println("DAO utilisateur");
			System.out.println(resultat);
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return resultat;

	}

	public Utilisateur connexionUtilisateur(String identifiant, String mdp, String recherche) {
		Utilisateur utilisateur = null;
		Connection cnx;
		String email = "SELECT *  FROM utilisateurs WHERE email = ? and mot_de_passe  = ?";
		String pseudo = "SELECT *  FROM utilisateurs WHERE pseudo = ? and mot_de_passe  = ?";
		String sql;
		if (recherche.equals("email")) {
			sql = email;
		} else {
			sql = pseudo;
		}
		try {
			cnx = ConnectionProvider.getConnection();

			PreparedStatement pstmt = cnx.prepareStatement(sql);
			pstmt.setString(1, identifiant);
			pstmt.setString(2, mdp);
			ResultSet rs = pstmt.executeQuery();

			if (rs.next()) {
				utilisateur = new Utilisateur(rs.getInt("no_utilisateur"), rs.getString("pseudo"), rs.getString("nom"),
						rs.getString("prenom"), rs.getString("email"), rs.getString("telephone"), rs.getString("rue"),
						rs.getString("code_postal"), rs.getString("ville"), rs.getInt("credit"),
						rs.getByte("administrateur"));
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return utilisateur;

	}

	public boolean verifPseudo(String pseudo) {
		Connection cnx;
		boolean resultat = false;

		try {
			cnx = ConnectionProvider.getConnection();

			PreparedStatement pstmt = cnx.prepareStatement("SELECT pseudo FROM utilisateurs WHERE pseudo = ?");
			pstmt.setString(1, pseudo);
			ResultSet rs = pstmt.executeQuery();
			System.out.println("DAO utilisateur 1");
			System.out.println(rs);
			if (rs.next()) {
				resultat = true;
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return resultat;
	}

	public void modificationUtilisateur(Utilisateur utilisateur) {
		Connection cnx;
		String sql = null;
		if (utilisateur != null) {
			System.out.println(utilisateur.getNoUtilisateur());
		} else {

			if (utilisateur.getMotDePasse() != null) {
				sql = "UPDATE utilisateurs SET nom = ?, prenom = ?, telephone = ?, rue = ?, ville = ?, code_postal = ?, mot_de_passe WHERE no_utilisateur = ?";
			} else {
				sql = "UPDATE utilisateurs SET nom = ?, prenom = ?, telephone = ?, rue = ?, ville = ?, code_postal = ? WHERE no_utilisateur = ?";
			}

			try {
				cnx = ConnectionProvider.getConnection();
				PreparedStatement pstmt = cnx.prepareStatement(sql);

				pstmt.setString(1, utilisateur.getNom());
				pstmt.setString(2, utilisateur.getPrenom());
				pstmt.setString(3, utilisateur.getTelephone());
				pstmt.setString(4, utilisateur.getRue());
				pstmt.setString(5, utilisateur.getCodePostal());
				pstmt.setString(6, utilisateur.getVille());
				if (utilisateur.getMotDePasse() != null) {
					pstmt.setString(7, utilisateur.getMotDePasse());
					pstmt.setInt(8, utilisateur.getNoUtilisateur());
				} else {
					pstmt.setInt(7, utilisateur.getNoUtilisateur());
				}

				pstmt.executeUpdate();
				cnx.close();

			} catch (SQLException e) {
				e.printStackTrace();
			}

		}
	}

	public Utilisateur recupereUtilisateurById(int id) {
		Connection cnx;
		Utilisateur utilisateur = null;
		try {
			cnx = ConnectionProvider.getConnection();

			PreparedStatement pstmt = cnx.prepareStatement("SELECT * FROM utilisateurs WHERE no_utilisateur = ?");
			pstmt.setInt(1, id);
			ResultSet rs = pstmt.executeQuery();

			if (rs.next()) {
				System.out.println(rs);
				utilisateur = new Utilisateur(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4),
						rs.getString(5), rs.getString(6), rs.getString(7), rs.getString(8), rs.getString(9),
						rs.getString(10), rs.getInt(11), rs.getByte(12));
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return utilisateur;
	}

}
