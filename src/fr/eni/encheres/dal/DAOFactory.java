package fr.eni.encheres.dal;

import fr.eni.encheres.dal.UtilisateurDAO;

public class DAOFactory {
	public static UtilisateurDAO getUtilisateurDAO() {
		return new UtilisateurDAO();
	}
}
