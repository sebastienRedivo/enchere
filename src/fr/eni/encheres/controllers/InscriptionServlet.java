package fr.eni.encheres.controllers;

import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.eni.encheres.bll.UtilisateurManager;
import fr.eni.encheres.bll.bo.Utilisateur;

/**
 * Servlet implementation class InscriptionServlet
 */
@WebServlet("/inscription")
public class InscriptionServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;


	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.getServletContext().getRequestDispatcher("/WEB-INF/inscription.jsp").forward(request, response);
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String pseudo = request.getParameter("pseudo");
		String mdp = request.getParameter("motDePasse");
		String email = request.getParameter("email");
		String nom = request.getParameter("nom");
		String prenom = request.getParameter("prenom");
		String telephone = request.getParameter("telephone");
		String rue = request.getParameter("rue");
		String codePostal = request.getParameter("codePostal");
		String ville = request.getParameter("ville");
		
		UtilisateurManager verifMailManager = new UtilisateurManager();
		UtilisateurManager verifPseudoManager = new UtilisateurManager();
		
		Pattern patternMail = Pattern.compile("^(.+)@(.+)$");
		Matcher matcherMail = patternMail.matcher(email);
		
		Pattern patternPseudo = Pattern.compile("^[a-zA-Z0-9]*$");
		Matcher matcherPseudo = patternPseudo.matcher(pseudo);
		
		
		if(!matcherMail.matches()) {
			request.setAttribute("message", "Email non conforme");
			request.setAttribute("alertMessage", "danger");
			request.getServletContext().getRequestDispatcher("/WEB-INF/inscription.jsp").forward(request, response);
		}
		if(!matcherPseudo.matches()) {
			request.setAttribute("message", "Pseudo non conforme, le pseudo doit contenir seulement des caract�res alphanum�rique sans espace ni caract�re sp�ciaux !");
			request.setAttribute("alertMessage", "danger");
			request.getServletContext().getRequestDispatcher("/WEB-INF/inscription.jsp").forward(request, response);
		}
		
		if(verifMailManager.verifMail(email)){
			request.setAttribute("message", "Email d�j� pr�sente");
			request.setAttribute("alertMessage", "danger");
			request.getServletContext().getRequestDispatcher("/WEB-INF/inscription.jsp").forward(request, response);
			
		}else if(verifPseudoManager.verifPseudo(pseudo)) {
			request.setAttribute("alertMessage", "danger");
			request.setAttribute("message", "Pseudo d�j� utilis�");
			request.getServletContext().getRequestDispatcher("/WEB-INF/inscription.jsp").forward(request, response);
			
		}else {
			UtilisateurManager um = new UtilisateurManager();
			Utilisateur utilisateur = new Utilisateur(pseudo, nom, prenom, email, telephone, rue, codePostal, ville, mdp);
			um.inscriptionUtilisateur(utilisateur);
			request.setAttribute("message", "Inscription r�ussi");
        	request.setAttribute("alertMessage", "success");
			request.getServletContext().getRequestDispatcher("/WEB-INF/inscription.jsp").forward(request, response);
		}
		
		
	}

}
