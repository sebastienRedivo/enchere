package fr.eni.encheres.controllers;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import fr.eni.encheres.bll.UtilisateurManager;
import fr.eni.encheres.bll.bo.Utilisateur;

/**
 * Servlet implementation class Profile
 */
@WebServlet(
		name = "profile",
		urlPatterns = "/utilisateur/profile"			
		)
public class Profile extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		UtilisateurManager utilisateurManagerManager = new UtilisateurManager();
		HttpSession session = request.getSession();
		Utilisateur res = (Utilisateur) session.getAttribute("utilisateurSession");
		System.out.println(res.getNoUtilisateur());
		//utilisateurManagerManager.getUtilisateurById(request.getSession().getAttribute("no_utilisateur"));
		
		request.getServletContext().getRequestDispatcher("/WEB-INF/utilisateur/profile.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
	
		doGet(request, response);
	}

}
