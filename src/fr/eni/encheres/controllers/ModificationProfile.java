package fr.eni.encheres.controllers;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import fr.eni.encheres.bll.UtilisateurManager;
import fr.eni.encheres.bll.bo.Utilisateur;

/**
 * Servlet implementation class ModificationProfile
 */
@WebServlet("/utilisateur/modificationProfile")
public class ModificationProfile extends HttpServlet {
	private static final long serialVersionUID = 1L;
       Utilisateur user = null;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		UtilisateurManager um = new UtilisateurManager();
		user = um.recupereUtilisateurById(6);
		System.out.println(user);
		request.getServletContext().getRequestDispatcher("/WEB-INF/utilisateur/modificationProfile.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String mdp = request.getParameter("motDePasse");
		String nom = request.getParameter("nom");
		String prenom = request.getParameter("prenom");
		String telephone = request.getParameter("telephone");
		String rue = request.getParameter("rue");
		String codePostal = request.getParameter("codePostal");
		String ville = request.getParameter("ville");
		HttpSession session = request.getSession();
		Utilisateur res = (Utilisateur) session.getAttribute("utilisateurSession");
		int id = (int) res.getNoUtilisateur();
		
		
		UtilisateurManager um = new UtilisateurManager();
		Utilisateur utilisateur = new Utilisateur(id, nom, prenom, telephone, rue, codePostal, ville, mdp);
		um.modificationUtilisateur(utilisateur);
		
		session.setAttribute("utilisateurSession", utilisateur);
		request.setAttribute("message", "Mise � jour r�ussi");
    	request.setAttribute("alertMessage", "success");
		request.getServletContext().getRequestDispatcher("/WEB-INF/utilisateur/profile.jsp").forward(request, response);
		
	}

}
