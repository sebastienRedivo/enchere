package fr.eni.encheres.controllers;

import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import fr.eni.encheres.bll.UtilisateurManager;
import fr.eni.encheres.bll.bo.Utilisateur;

/**
 * Servlet implementation class Connexion
 */
@WebServlet("/connexion")
public class Connexion extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//session
		HttpSession session = request.getSession();
		int compteurAcces = 0;
		if(session.getAttribute("compteurAcces") != null) {
			compteurAcces = (int) session.getAttribute("compteurAcces");
		}
		compteurAcces+=1;
		session.setAttribute("compteurAcces", compteurAcces);
		
		request.getServletContext().getRequestDispatcher("/WEB-INF/connexion.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String identifiant;
		String mdp;
		Cookie[] cookies = request.getCookies();
		if(cookies == null) {
			 identifiant = request.getParameter("identifiant");
			 mdp = request.getParameter("motDePasse");
		}else {
			
			
			 identifiant = request.getParameter("identifiant");
			 mdp = request.getParameter("motDePasse");
		}
		
		String resteConnecter = null; 
		String recherche = null;
		
		// permet de v�rifier l'email si il est conforme
		
		Pattern pattern = Pattern.compile("^(.+)@(.+)$");
		Matcher matcher = pattern.matcher(identifiant);
		

		if(identifiant != null && mdp != null) {
			UtilisateurManager um = new UtilisateurManager();
			
			if (matcher.matches()) {
				recherche = "email";
			} else {
				recherche = "pseudo";
			}
			
			Utilisateur utilisateur = um.connexionUtilisateur(identifiant, mdp, recherche);
			
			if(utilisateur == null) {
				request.setAttribute("message", "identifiant ou mot de passe incorrect");
	        	request.setAttribute("alertMessage", "danger");
				request.getServletContext().getRequestDispatcher("/WEB-INF/index.jsp").forward(request, response);
			}else {
				
				
				HttpSession session = request.getSession();
				session.setAttribute("utilisateurSession", utilisateur);
				request.setAttribute("utilisateur", utilisateur);
				request.setAttribute("message", "connexion r�ussi");
	        	request.setAttribute("alertMessage", "success");
				//request.getServletContext().getRequestDispatcher("/WEB-INF/index.jsp").forward(request, response);
				response.sendRedirect(request.getContextPath() + "/index");
				
			}
			
		}
		
	}

}
