<section class="container pt-5">

		<form action="modificationProfile" method="post" class="row">
		<div class="mb-3 col-sm-6 col-xs-12">
			<label for="pseudo" class="form-label">Pseudo</label> <input
				type="text" name="pseudo" id="pseudo" placeholder="pseudo..."
				value="${ param.pseudo }" class="form-control">
		</div>
		<div class="mb-3 col-sm-6 col-xs-12">
			<label for="email" class="form-label">Email</label> <input
				type="email" name="email" id="email" placeholder="email@domaine.fr"
				value="${ param.email }" class="form-control">
		</div>
		<div class="mb-3 col-sm-6 col-xs-12">
			<label for="nom" class="form-label">Nom</label> <input type="text"
				name="nom" id="nom" placeholder="nom" value="${ param.nom }"
				class="form-control">
		</div>
		<div class="mb-3 col-sm-6 col-xs-12">
			<label for="prenom" class="form-label">Pr�nom</label> <input
				type="text" name="prenom" id="prenom" placeholder="Pr�nom"
				value="${ param.prenom }" class="form-control">
		</div>
		<div class="mb-3 col-sm-6 col-xs-12">
			<label for="rue" class="form-label">Rue</label> <input type="text"
				name="rue" id="rue" placeholder="" value="${ param.rue }"
				class="form-control">
		</div>
		<div class="mb-3 col-sm-6 col-xs-12">
			<label for="codePostal" class="form-label">Code postal</label> <input
				type="text" name="codePostal" id="codePostal" placeholder=""
				value="${ param.codePostal }" class="form-control">
		</div>
		<div class="mb-3 col-sm-6 col-xs-12">
			<label for="ville" class="form-label">Ville</label> <input
				type="text" name="ville" id="ville" placeholder=""
				value="${ param.ville }" class="form-control">
		</div>
		<div class="mb-3 col-sm-6 col-xs-12">
			<label for="telephone" class="form-label">T�l�phone</label> <input
				type="tel" name="telephone" id="telephone" placeholder="0700123456"
				value="${ param.telephone }" class="form-control">
		</div>

		<div class="mb-3 col-sm-6 col-xs-12">
			<div class="">
				<label for="motDePasse" class="form-label">Mot de passe</label> <input
					type="password" id="motDePasse" class="form-control mb-2"
					aria-describedby="motDePasseAide" name="motDePasse"
					placeholder="mot de passe"> <input type="password"
					id="confirm_motDePasse" class="form-control mb-2"
					aria-describedby="motDePasseAide" name="confirm_motDePasse"
					placeholder="confirmez mot de passe">
			</div>
		</div>
		<div class="col-12 flex">
				<button class="btn btn-primary"><c:if test="${sessionScope.utilisateurSession != null}">
		<h1>Mettre � jour</h1>
	</c:if>
		<c:if test="${sessionScope.utilisateurSession == null}">
		<h1>s'inscrire</h1>
	</c:if>
	</button>
				<input type="reset" class="btn btn-secondary"></button>
			</div>

	</form>
</section>