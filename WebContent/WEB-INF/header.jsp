<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>

<meta charset="UTF-8">

<link rel="stylesheet"
	href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css"
	integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l"
	crossorigin="anonymous">
<title><%=request.getParameter("titrePage")%></title>
</head>
<body>
	<header class="mb-5">
		<nav
			class="navbar fixed-top navbar-expand-lg navbar-dark  bg-dark text-white">
			<a class="navbar-brand" href="index"><strong>Enchere.org</strong></a>
			<button class="navbar-toggler" type="button" data-toggle="collapse"
				data-target="#navbarSupportedContent"
				aria-controls="navbarSupportedContent" aria-expanded="false"
				aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>
			<div class="collapse navbar-collapse" id="navbarSupportedContent">
				<ul class="navbar-nav mr-auto">
					<li class="nav-item active"><a class="nav-link"
						href="${pageContext.request.contextPath}/index">Acceuil </a></li>
					<li class="nav-item"><a class="nav-link"
						href="${pageContext.request.contextPath}/inscription">Inscription</a>
					</li>
					<span class="align-middle">-</span>
					<li class="nav-item"><a class="nav-link"
						href="${pageContext.request.contextPath}/utilisateur/profile">Profile</a>
					</li>
				</ul>
				<ul class="navbar-nav">
					<c:choose>
						<c:when test="${empty sessionScope.utilisateurSession}">
							<li class="nav-item"><a class="nav-link"
								href="${pageContext.request.contextPath}/inscriptionConnexion">inscription/connexion
							</a></li>
						</c:when>
						<c:when test="${sessionScope.utilisateurSession != null}">
							<li class="nav-item"><a class="nav-link"
								href="${pageContext.request.contextPath}/deconnexion">Déconnexion</a>
							</li>
						</c:when>
					</c:choose>
				</ul>
			</div>
		</nav>
	</header>
	<c:if test="${message != null}">
		<div class="container alert alert-${ alertMessage } ">${ message }
		</div>
	</c:if>