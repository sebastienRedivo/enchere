<jsp:include page="../header.jsp" >
<jsp:param name="titrePage" value="profile" />
</jsp:include>
<section class="container pt-5">         
  <table class="table table-striped">
    <thead>
    </thead>
    <tbody>
    <tr>
        <td>email :</td>
        <td>${ sessionScope.utilisateurSession.email }</td>
      </tr>
      <tr>
        <td>Pseudo :</td>
        <td>${ sessionScope.utilisateurSession.pseudo }</td>
      </tr>
      <tr>
        <td>Nom :</td>
        <td>${ sessionScope.utilisateurSession.nom }</td>
      </tr>
      <tr>
        <td>Pr�nom :</td>
        <td>${ sessionScope.utilisateurSession.prenom }</td>
      </tr>
      <tr>
        <td>Adresse :</td>
        <td>${ sessionScope.utilisateurSession.rue } ${ sessionScope.utilisateurSession.codePostal } ${ sessionScope.utilisateurSession.ville }</td>
      </tr>
      <tr>
        <td>T�l�phone :</td>
        <td>${ sessionScope.utilisateurSession.telephone }</td>
      </tr>
      <tr>
        <td>Cr�dit :</td>
        <td>${ sessionScope.utilisateurSession.credit }</td>
      </tr>
    </tbody>
  </table>
		<a href="${pageContext.request.contextPath}/utilisateur/modificationProfile" class="btn btn-secondary">update</a>
	
	
</section>

<jsp:include page="../footer.jsp" />