<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<jsp:include page="../header.jsp">
	<jsp:param name="titrePage" value="modification du profile" />
</jsp:include>
<section class="container pt-5">

		<form action="modificationProfile" method="post" class="row">
		<div class="mb-3 col-sm-6 col-xs-12">
			<label for="pseudo" class="form-label">Pseudo</label> <input
				type="text" name="pseudo" id="pseudo" placeholder="pseudo..."
				value="${ sessionScope.utilisateurSession.pseudo }" class="form-control">
		</div>
		<div class="mb-3 col-sm-6 col-xs-12">
			<label for="email" class="form-label">Email</label> <input
				type="email" name="email" id="email" placeholder="email@domaine.fr"
				value="${ sessionScope.utilisateurSession.email }" class="form-control">
		</div>
		<div class="mb-3 col-sm-6 col-xs-12">
			<label for="nom" class="form-label">Nom</label> <input type="text"
				name="nom" id="nom" placeholder="nom" value="${ sessionScope.utilisateurSession.nom }"
				class="form-control">
		</div>
		<div class="mb-3 col-sm-6 col-xs-12">
			<label for="prenom" class="form-label">Pr�nom</label> <input
				type="text" name="prenom" id="prenom" placeholder="Pr�nom"
				value="${ sessionScope.utilisateurSession.prenom }" class="form-control">
		</div>
		<div class="mb-3 col-sm-6 col-xs-12">
			<label for="rue" class="form-label">Rue</label> <input type="text"
				name="rue" id="rue" placeholder="" value="${ sessionScope.utilisateurSession.rue }"
				class="form-control">
		</div>
		<div class="mb-3 col-sm-6 col-xs-12">
			<label for="codePostal" class="form-label">Code postal</label> <input
				type="text" name="codePostal" id="codePostal" placeholder=""
				value="${ sessionScope.utilisateurSession.codePostal }" class="form-control">
		</div>
		<div class="mb-3 col-sm-6 col-xs-12">
			<label for="ville" class="form-label">Ville</label> <input
				type="text" name="ville" id="ville" placeholder=""
				value="${ sessionScope.utilisateurSession.ville }" class="form-control">
		</div>
		<div class="mb-3 col-sm-6 col-xs-12">
			<label for="telephone" class="form-label">T�l�phone</label> <input
				type="tel" name="telephone" id="telephone" placeholder="0700123456"
				value="${ sessionScope.utilisateurSession.telephone }" class="form-control">
		</div>

		<div class="mb-3 col-sm-6 col-xs-12">
			<div class="">
				<label for="motDePasse" class="form-label">Mot de passe</label> <input
					type="password" id="motDePasse" class="form-control mb-2"
					aria-describedby="motDePasseAide" name="motDePasse"
					placeholder="mot de passe"> <input type="password"
					id="confirm_motDePasse" class="form-control mb-2"
					aria-describedby="motDePasseAide" name="confirm_motDePasse"
					placeholder="confirmez mot de passe">
			</div>
		</div>
			<div class="col-12 flex">
				<button class="btn btn-primary">
					Mettre � jour
				</button>
				<a href="${pageContext.request.contextPath}/utilisateur/profile" class="btn btn-secondary">retour</a>
			</div>

	</form>
</section>

<jsp:include page="../footer.jsp" />