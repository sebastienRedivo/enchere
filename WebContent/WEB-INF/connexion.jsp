<jsp:include page="header.jsp">
	<jsp:param name="titrePage" value="Se connecter" />
</jsp:include>

<section class="container pt-5">

	<form action="connexion" method="POST" class="row">
		<div class="card card-signin flex-row m-auto col-sm-8 col-xs-12">
			<div class="card-body">
				<div class="mb-3 col-sm-12 col-xs-12">
					<label for="identifiant" class="form-label">Identifiant</label> <input
						type="text" name="identifiant" id="identifiant"
						placeholder="email / pseudo" class="form-control">
				</div>
				<div class="mb-3 col-sm-12 col-xs-12">
					<div class="">
						<label for="motDePasse" class="form-label">Mot de passe</label> <input
							type="password" id="motDePasse" class="form-control mb-2"
							aria-describedby="motDePasseAide" name="motDePasse"
							placeholder="mot de passe">
					</div>

				</div>
				<div class="mb-3 col-sm-12 col-xs-12">
				<button class="btn btn-success ">Se
					connecter</button>
				</div>
			</div>
		</div>
	</form>
</section>

<jsp:include page="footer.jsp" />