<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<jsp:include page="header.jsp">
	<jsp:param name="titrePage" value="inscription connexion" />
</jsp:include>

<section class="container pt-5">
	<div class="row">
			<div class="card card-signin flex-row m-3 col-sm-5 col-xs-12">
				<div class="card-body">

					<h5 class="card-title text-center">S'inscrire</h5>
						<a class="btn btn-lg btn-primary btn-block text-uppercase" href="inscription">S'inscrire</a>
						<hr class="my-4">
					
				</div>
			</div>

		<div class="card  flex-row m-3 col-sm-5 col-xs-12">
				<div class="card-body">

					<h5 class="card-title text-center">Connexion</h5>
						<a class="btn btn-lg btn-primary btn-block text-uppercase" href="connexion">Se connecter</a>
						<hr class="my-4">
					
				</div>
			</div>
	</div>
</section>

<jsp:include page="footer.jsp" />

